# TODO

* Show somehow player death and armadillo destruction
* Feedback when enemy is attacking or digging
* Balancing
    * I think I need less earth walls in the starting levels
    * ~~Maybe I don't want lights casting shadows: it reduces the benefit of oil!~~
* Visual effects: particles, etc
* Player animation
* Gamepad support

## Done

* ~~Armadillo: shape~~
* ~~Basic HUD~~
* ~~Basic level: empty room~~
* ~~Armadillo: next level timeout~~
* ~~Player walking~~
* ~~Player collides with walls~~
* ~~Player needs to be at the Armadillo when digging timer zeroes~~
* ~~Earth blocks~~
* ~~Add variety to levels~~
* ~~Destructible blocks~~
* ~~Lighting (underground is dark, player and Armadillo have lights)~~
* ~~Game end after N levels~~
* ~~Player health~~
* ~~Player can die~~
* ~~Armadillo can be destroyed~~
* ~~Basic enemy~~
* ~~Add resources: Ore, Oil~~
* ~~Make resources pickable~~
* ~~Lamp radius proportional to amount of oil (which decreases)~~
    * ~~And maybe remove Armadillo light~~
* ~~Upgrades: armor, weapon, pick, drill~~
* ~~Implement function to set drill time~~
* ~~Show player direction~~
* ~~Give feedback when using the pick~~
* ~~Player weapon~~
* ~~Impassable blocks look terrible, fix this~~
* ~~Change texture of impassable blocks and ground (they look like cracks)~~
* ~~Add some sound effects~~
* ~~Add some music~~
* ~~Create title screen~~
* ~~Create help screen~~
* ~~Game over screen~~
* ~~Allow to return from game over and victory screens to the title screen~~
* ~~Make a nice game over screen (maybe one for each possible failure)~~
* ~~Make a nice victory screen~~
* ~~End level: "Welcome to the end of the rabbit hole", gift shop, etc~~
* ~~ Visual effects: shaking Armadillo~~
