# LD 48: Down the Rabbit Hole

My entry for Ludum Dare 48; theme is "Deeper and Deeper".

License is GPL3.

## Credits

* Engine: [Godot](http://godotengine.org) 3.3.
* Graphics: [Inkscape](https://inkscape.org) 1.0.2.
* Font: [Lato](http://www.latofonts.com), by Łukasz Dziedzic.
* Sound effects: [SFXR](http://www.drpetter.se/project_sfxr.html) and
  [Audacity](https://www.audacityteam.org)
* Music: [1BITDRAGON](https://1bitdragon.itch.io/1bitdragon).
* Noise blocking by [myNoise](https://mynoise.net).

## Notes to self

### Positioning things on the level

* Not using Godot's tile maps (not until Godot 4.0, at least 😉)
* Tiles are 80-pixel squares.
* Objects are centered around the origin.
* Things are placed on multiples of 80 on the map.
* Level centered around the origin (0,0).
* Armadillo is always at (0, 0), it never moves.

### Screen

* Game window: 1280 x 720
