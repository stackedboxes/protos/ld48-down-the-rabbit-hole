extends CanvasLayer


func _process(_delta: float) -> void:
	$Level/Value.text = str(G.level)
	$DiggingTime/Value.text = _formatTime(G.diggingTimeInSecs)
	$PlayerHP/Value.text = _formatPlayerHP()
	$ArmadilloHP/Value.text = _formatArmadilloHP()
	$Ore/Value.text = str(G.ore)
	$Oil/Value.text = "%.1f" % G.oil


func _formatTime(timeInSecs: int) -> String:
	var mins = timeInSecs / 60
	var secs = timeInSecs - mins * 60
	return "%02d:%02d" % [mins, secs]


func _formatPlayerHP() -> String:
	return "%d / %d" % [int(G.playerHP), int(G.maxPlayerHP)]


func _formatArmadilloHP() -> String:
	return "%d / %d" % [int(G.armadilloHP), int(G.maxArmadilloHP)]


func doArmadilloAlarm() -> void:
	$AnimationPlayer.play("ArmadilloAlarm")
