extends BaseLevel

# Lots of weird syntax here; bug in 3.3 GDScript?

func _ready() -> void:
	G.resetGlobalStateForNewGame()


func _generateLevel(depth: int) -> void:
	._generateEmptyLevel()
	_addEnemy()
#	_addPassableWallAt(8,9)
#	_addPassableWallAt(8,7)
#	_addPassableWallAt(7,8)
#	_addPassableWallAt(9,8)


func _addEnemy() -> void:
	var sz = G.TILE_SIZE
	var x := 8
	var y := 8

	var bug = self.bugScene.instance()
	bug.position = Vector2(x*sz, y*sz)
	bug.setLevel(self)
	bug.gridPosition = Vector2(x, y)
	.get_node("Enemies").add_child(bug)
	.setEnemyAt(x, y, bug)


func _addPassableWallAt(x: int, y: int) -> void:
	var sz = G.TILE_SIZE
	var earth = self.earthScene.instance()
	earth.position = Vector2(x*sz, y*sz)
	.get_node("Walls").add_child(earth)
	._setWallAt(x, y, earth)
