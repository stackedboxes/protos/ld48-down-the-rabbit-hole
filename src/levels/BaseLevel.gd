tool
extends Node2D
class_name BaseLevel

onready var groundScene: PackedScene = preload("res://levels/GroundTile.tscn")
onready var impassableScene: PackedScene = preload("res://levels/ImpassableWall.tscn")
onready var earthScene: PackedScene = preload("res://levels/EarthWall.tscn")

onready var bugScene: PackedScene = preload("res://enemies/Bug.tscn")

onready var oreScene: PackedScene = preload("res://items/OreResource.tscn")
onready var oilScene: PackedScene = preload("res://items/OilResource.tscn")

export var HalfWidth: int = 16
export var HalfHeight: int = 10

var _walls := [] # indexed by x, y
var _enemies := [] # indexed by x, y
var _resources := [] # indexed by x, y
var _playerGridPos = Vector2(0, 0)


func _ready() -> void:
	_generateLevel(G.level)


func setPlayerGridPos(pos: Vector2) -> void:
	_playerGridPos = pos


func getPlayerGridPos() -> Vector2:
	return _playerGridPos


func getArmadilloGridPos() -> Vector2:
	return Vector2(0, 0)


func isWalkable(pos: Vector2) -> bool:
	return _wallAt(pos) == null && enemyAt(pos) == null && _playerGridPos.distance_squared_to(pos) > 0.0


func isDiggable(pos: Vector2) -> bool:
	var wall = _wallAt(pos)
	return wall != null && wall.destructible


func playerDigAt(target: Vector2, delta: float) -> int:
	var wall = _wallAt(target)
	if wall == null:
		return G.DigResult.HIT_NOTHING

	if !wall.destructible:
		return G.DigResult.HIT_INDESTRUCTIBLE

	var destroyed = wall.hit(G.pickDamagePerSec * delta)
	if destroyed:
		$WallDestroyedPlayer.play()
		wall.queue_free()
		_setWallAt(target, null)
		return G.DigResult.HIT_AND_DESTROYED
	else:
		return G.DigResult.HIT_DESTRUCTIBLE


func playerAttackAt(target: Vector2, delta: float) -> int:
	var wall = _wallAt(target)
	if wall != null:
		return G.AttackResult.HIT_WALL

	var enemy = enemyAt(target)
	if enemy == null:
		return G.AttackResult.HIT_NOTHING

	var killed = enemy.hit(G.knifeDamagePerSec * delta)
	if killed:
		$EnemyKilledPlayer.play()
		enemy.queue_free()
		setEnemyAt(target, null)
		return G.AttackResult.HIT_AND_KILLED
	else:
		return G.AttackResult.HIT_ENEMY


func enemyDigAt(target: Vector2, damage: float) -> int:
	var wall = _wallAt(target)
	if wall == null:
		return G.DigResult.HIT_NOTHING

	if !wall.destructible:
		return G.DigResult.HIT_INDESTRUCTIBLE # safe, but should never happen

	var destroyed = wall.hit(damage)
	if destroyed:
		$WallDestroyedPlayer.play()
		wall.queue_free()
		_setWallAt(target, null)
		return G.DigResult.HIT_AND_DESTROYED
	else:
		return G.DigResult.HIT_DESTRUCTIBLE


func enemyAttackAt(target: Vector2, damage: float) -> int:
	var wall = _wallAt(target)
	if wall != null:
		return G.EnemyAttackResult.HIT_WALL

	if G.armadilloHP > 0.0 && getArmadilloGridPos().distance_squared_to(target) < 0.1:
		G.armadilloHP -= damage
		if G.armadilloHP <= 0.0:
			SceneStack.pop(G.LevelResult.ARMADILLO_DESTROYED)
		return G.EnemyAttackResult.HIT_ARMADILLO

	if G.playerHP > 0.0 && getPlayerGridPos().distance_squared_to(target) < 0.1:
		G.playerHP -= damage
		if G.playerHP <= 0.0:
			SceneStack.pop(G.LevelResult.PLAYER_KILLED)
		return G.EnemyAttackResult.HIT_PLAYER

	return G.EnemyAttackResult.HIT_NOTHING


func getResourceMaybe(position: Vector2) -> bool:
	var resource := resourceAt(position)
	if resource == null:
		return false

	$ResourceTakenPlayer.play()
	resource.taken()
	resource.queue_free()
	setResourceAt(position, null)
	return true


func _generateLevel(depth: int) -> void:
	_generateEmptyLevel()
	_addImpassableWalls(depth)
	_addPassableWalls(depth)
	_addResources(depth)
	_addEnemies(depth)


func _generateEmptyLevel() -> void:
	var sz = G.TILE_SIZE
	for x in range(-HalfWidth, HalfWidth+1):
		_walls.push_back([])
		_enemies.push_back([])
		_resources.push_back([])
		for y in range(-HalfHeight, HalfHeight+1):
			_enemies[x+HalfWidth].push_back(null)
			_resources[x+HalfWidth].push_back(null)
			var ground: Node2D = groundScene.instance()
			ground.position = Vector2(x*sz, y*sz)
			$Ground.add_child(ground)
			if abs(x) == HalfWidth || abs(y) == HalfHeight:
				var impassable = impassableScene.instance()
				impassable.position = Vector2(x*sz, y*sz)
				$Walls.add_child(impassable)
				_walls[x+HalfWidth].push_back(impassable)
			else:
				_walls[x+HalfWidth].push_back(null)


func _addImpassableWalls(_depth: int) -> void:
	var sz = G.TILE_SIZE
	var density = RNG.uniform(0.1, 0.9)
	var deltaDensity = RNG.uniform(-0.1, 0.1)
	for y in range(-HalfHeight+2, HalfHeight+1, 2):
		deltaDensity += RNG.uniform(-0.05, 0.05)
		deltaDensity = clamp(deltaDensity, -0.2, 0.2)
		density += deltaDensity
		density = clamp(density, 0.1, 0.9)
		for x in range(-HalfWidth, HalfWidth+1):
			# no impassable walls too close of the Armadillo
			if Vector2(x, y).length() < 3.0:
				continue

			# add impassable walls maybe
			if RNG.bernoulli(density):
				var impassable = impassableScene.instance()
				impassable.position = Vector2(x*sz, y*sz)
				$Walls.add_child(impassable)
				_setWallAt(x, y, impassable)


func _addPassableWalls(_depth: int) -> void:
	var sz = G.TILE_SIZE
	var density = RNG.uniform(0.5, 1.0)
	var deltaDensity = RNG.uniform(-0.1, 0.1)
	for y in range(-HalfHeight+1, HalfHeight+1):
		deltaDensity += RNG.uniform(-0.05, 0.05)
		deltaDensity = clamp(deltaDensity, -0.2, 0.2)
		density += deltaDensity
		density = clamp(density, 0.25, 1.0)
		for x in range(-HalfWidth, HalfWidth+1):
			# no walls too close of the Armadillo
			if Vector2(x, y).length() < 2.5:
				continue

			# no double walls
			if _wallAt(x, y) != null:
				continue

			# add passable walls maybe
			if RNG.bernoulli(density):
				var earth = earthScene.instance()
				earth.position = Vector2(x*sz, y*sz)
				$Walls.add_child(earth)
				_setWallAt(x, y, earth)


func _addResources(depth: int) -> void:
	var sz = G.TILE_SIZE
	var resourcesToAdd = 50 + depth * 5
	var oreProb = 2.0/3.0
	var maxLoops = 5000
	while resourcesToAdd > 0:
		maxLoops -= 1
		if maxLoops < 0:
			break

		var x := RNG.uniformInt(-HalfWidth, HalfWidth)
		var y := RNG.uniformInt(-HalfHeight, HalfHeight)

		# no resources too close of the Armadillo or in unreachable places
		if Vector2(x, y).length() < 1.5:
			continue

		var wall := _wallAt(x, y)
		if wall != null && !wall.destructible:
			continue

		# no multiple resources per tile
		if resourceAt(x, y) != null:
			continue

		# add resource
		var resource := oreScene.instance() if RNG.bernoulli(oreProb) else oilScene.instance()
		resource.position = Vector2(x*sz, y*sz)
		$Resources.add_child(resource)
		setResourceAt(x, y, resource)
		resourcesToAdd -= 1


func _addEnemies(depth: int) -> void:
	var sz = G.TILE_SIZE
	var enemiesToAdd = depth * 3
	var maxLoops = 5000
	while enemiesToAdd > 0:
		maxLoops -= 1
		if maxLoops < 0:
			break

		var x := RNG.uniformInt(-HalfWidth, HalfWidth)
		var y := RNG.uniformInt(-HalfHeight, HalfHeight)

		# no enemies too close of the Armadillo or in occupied places
		if Vector2(x, y).length() < 4.0:
			continue

		if _wallAt(x, y) != null || enemyAt(x, y) != null:
			continue

		# add enemy
		var bug = bugScene.instance()
		bug.position = Vector2(x*sz, y*sz)
		bug.setLevel(self)
		bug.gridPosition = Vector2(x, y)
		$Enemies.add_child(bug)
		setEnemyAt(x, y, bug)
		enemiesToAdd -= 1


func _wallAt(x, y = null) -> BaseWall:
	match typeof(x):
		TYPE_INT:
			return _walls[x+HalfWidth][y+HalfHeight]
		TYPE_REAL:
			return _walls[int(x+HalfWidth)][int(y+HalfHeight)]
		TYPE_VECTOR2:
			return _walls[int(x.x+HalfWidth)][int(x.y+HalfHeight)]
		_:
			assert(false, "Can't happen")
			return null


func _setWallAt(aa, bb, cc = null):
	match typeof(aa):
		TYPE_INT:
			_walls[aa+HalfWidth][bb+HalfHeight] = cc
		TYPE_REAL:
			_walls[int(aa+HalfWidth)][int(bb+HalfHeight)] = cc
		TYPE_VECTOR2:
			_walls[int(aa.x+HalfWidth)][int(aa.y+HalfHeight)] = bb
		_:
			assert(false, "Can't happen")


func resourceAt(x, y = null) -> BaseResource:
	match typeof(x):
		TYPE_INT:
			return _resources[x+HalfWidth][y+HalfHeight]
		TYPE_REAL:
			return _resources[int(x+HalfWidth)][int(y+HalfHeight)]
		TYPE_VECTOR2:
			return _resources[int(x.x+HalfWidth)][int(x.y+HalfHeight)]
		_:
			assert(false, "Can't happen")
			return null


func setResourceAt(aa, bb, cc = null):
	match typeof(aa):
		TYPE_INT:
			_resources[aa+HalfWidth][bb+HalfHeight] = cc
		TYPE_REAL:
			_resources[int(aa+HalfWidth)][int(bb+HalfHeight)] = cc
		TYPE_VECTOR2:
			_resources[int(aa.x+HalfWidth)][int(aa.y+HalfHeight)] = bb
		_:
			assert(false, "Can't happen")


func enemyAt(x, y = null) -> BaseEnemy:
	match typeof(x):
		TYPE_INT:
			return _enemies[x+HalfWidth][y+HalfHeight]
		TYPE_REAL:
			return _enemies[int(x+HalfWidth)][int(y+HalfHeight)]
		TYPE_VECTOR2:
			return _enemies[int(x.x+HalfWidth)][int(x.y+HalfHeight)]
		_:
			assert(false, "Can't happen")
			return null



func setEnemyAt(aa, bb, cc = null):
	match typeof(aa):
		TYPE_INT:
			_enemies[aa+HalfWidth][bb+HalfHeight] = cc
		TYPE_REAL:
			_enemies[int(aa+HalfWidth)][int(bb+HalfHeight)] = cc
		TYPE_VECTOR2:
			_enemies[int(aa.x+HalfWidth)][int(aa.y+HalfHeight)] = bb
		_:
			assert(false, "Can't happen")
