extends Node2D
class_name BaseWall

export var impassable := false
export var destructible := false
export var maxHP := -10

onready var _crackImages = [
	preload("res://levels/cracks-5.svg"),
	preload("res://levels/cracks-4.svg"),
	preload("res://levels/cracks-3.svg"),
	preload("res://levels/cracks-2.svg"),
	preload("res://levels/cracks-1.svg"),
	preload("res://levels/cracks-0.svg"),
]

var _hp := 0.0


func _ready() -> void:
	if destructible:
		_hp = maxHP
	_updateCracks()


# Returns Boolean telling if the block was destroyed.
func hit(damage: float) -> bool:
	_hp -= damage
	_updateCracks()
	return _hp <= 0.0


func _updateCracks() -> void:
	if !destructible:
		$Sprite/Cracks.texture = _crackImages[len(_crackImages) - 1]
		return

	var p := _hp / maxHP
	var i := int(len(_crackImages) * p)
	i = clamp(i, 0, len(_crackImages) - 1)
	$Sprite/Cracks.texture = _crackImages[i]
