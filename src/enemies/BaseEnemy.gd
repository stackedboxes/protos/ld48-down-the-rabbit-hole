extends Node2D
class_name BaseEnemy

export var _hp: float = 10
export var _speed: float = 100
export var _digPerSec: int = 2
export var _damagePerSec: int = 2

var _level # BaseLevel, but circular dependency :-(

var _direction = G.Direction.EAST


func _process(delta: float) -> void:
	_processAI(delta)


func setLevel(level) -> void:
	_level = level


# As in "this enemy was hit". Returns Boolean telling if the enemy was killed.
func hit(damage: float) -> bool:
	# Counter-attack!
	_mode = Mode.ATTACKING_PLAYER
	_decideAgainInSecs = clamp(_decideAgainInSecs, 5.0, DECIDE_EVERY_SECS)

	# Get hurt
	_hp -= damage
	return _hp <= 0.0


func _dirToVec(dir) -> Vector2:
	match dir:
		G.Direction.NORTH:
			return Vector2(0, -1)
		G.Direction.SOUTH:
			return Vector2(0, 1)
		G.Direction.EAST:
			return Vector2(1, 0)
		G.Direction.WEST:
			return Vector2(-1, 0)
		_:
			assert(false, "Can't happen")
			return Vector2(0, 0)


# In grid units
func _distToPlayer() -> float:
	var playerGridPos = _level.getPlayerGridPos()
	var myGridPos := position / G.TILE_SIZE
	return myGridPos.distance_to(playerGridPos)


# In grid units
func _distToArmadillo() -> float:
	var myGridPos := position / G.TILE_SIZE
	return myGridPos.length()


const DECIDE_EVERY_SECS = 10.0
const PERCEPTION_RADIUS = 4.5

enum Mode {
	WANDERING,
	ATTACKING_PLAYER,
	ATTACKING_ARMADILLO,
	TRACKING_ARMADILLO,
}

enum State {
	IDLE,
	WALKING,
	ATTACKING,
	DIGGING
}

var _mode = Mode.WANDERING
var _state = State.IDLE
var _decideAgainInSecs := 0.0

var gridPosition := Vector2(0, 0) # integer indices into grid


func _processAI(delta: float) -> void:
	_decideAgainInSecs -= delta
	if _decideAgainInSecs < 0.0:
		_mode = _decideWhatToDo()
		_decideAgainInSecs = DECIDE_EVERY_SECS

	match _state:
		State.IDLE:
			_processStateIdle(delta)
		State.WALKING:
			_processStateWalking(delta)
		State.ATTACKING:
			_processStateAttacking(delta)
		State.DIGGING:
			_processStateDigging(delta)
		_:
			assert(false, "Can't happen")


# Returns next desired mode
func _decideWhatToDo() -> int:
	var distToPlayer = _distToPlayer()
	var distToArmadillo = _distToArmadillo()

	if distToPlayer < PERCEPTION_RADIUS && distToArmadillo < PERCEPTION_RADIUS:
		if RNG.flipCoin():
			return Mode.ATTACKING_ARMADILLO
		return Mode.ATTACKING_PLAYER

	if distToPlayer < PERCEPTION_RADIUS:
		return Mode.ATTACKING_PLAYER

	if distToArmadillo < PERCEPTION_RADIUS:
		return Mode.ATTACKING_ARMADILLO

	if RNG.bernoulli(2.0/3.0):
		return Mode.TRACKING_ARMADILLO

	return Mode.WANDERING


func _processStateIdle(_delta: float) -> void:
	match _mode:
		Mode.WANDERING:
			_nextActionWandering()
		Mode.ATTACKING_PLAYER:
			_nextActionAttackingPlayer()
		Mode.ATTACKING_ARMADILLO:
			_nextActionAttackingArmadillo()
		Mode.TRACKING_ARMADILLO:
			_nextActionTrackingArmadillo()
		_:
			assert(false, "Can't happen")


func _processStateWalking(delta: float) -> void:
	var target := gridPosition * G.TILE_SIZE
	var move: Vector2 = (target - position).normalized() * _speed * delta
	var squaredDist = position.distance_squared_to(target)
	if squaredDist < move.length_squared():
		position = target
		_state = State.IDLE
	else:
		position += move


func _processStateAttacking(delta: float) -> void:
	var target = gridPosition + _dirToVec(_direction)
	var result = _level.enemyAttackAt(target, _damagePerSec * delta)
	if result != G.EnemyAttackResult.HIT_PLAYER && result != G.EnemyAttackResult.HIT_ARMADILLO:
		_state = State.IDLE


func _processStateDigging(delta: float) -> void:
	var target = gridPosition + _dirToVec(_direction)
	var result = _level.enemyDigAt(target, delta * _digPerSec)
	if result != G.DigResult.HIT_DESTRUCTIBLE:
		_state = State.IDLE
		_decideAgainInSecs = 0.0


func _nextActionWandering() -> void:
	var dirs = RNG.shuffle([ G.Direction.NORTH, G.Direction.SOUTH,
		G.Direction.EAST, G.Direction.WEST])

	for dir in dirs:
		var dirVec := _dirToVec(dir)
		var targetPos = gridPosition + dirVec
		if isWalkableForEnemy(targetPos):
			_startWalking(dir)
			return

	# cannot move, decide again
	_decideAgainInSecs = 0.0


func _nextActionAttackingPlayer() -> void:
	_nextActionAttackingThisPoint(_level.getPlayerGridPos())


func _nextActionAttackingArmadillo() -> void:
	_nextActionAttackingThisPoint(_level.getArmadilloGridPos())


func _nextActionTrackingArmadillo() -> void:
	if _moveTowardsPointIfAway(_level.getArmadilloGridPos()):
		return
	_decideAgainInSecs = 0.0


func _startWalking(dir) -> void:
	_direction = dir
	var dirVec := _dirToVec(dir)
	var targetPos = gridPosition + dirVec

	assert(_level.enemyAt(targetPos) == null, "Inconsistent map state")
	assert(_level.enemyAt(gridPosition) == self, "Inconsistent map state")
	_level.setEnemyAt(gridPosition, null)
	_level.setEnemyAt(targetPos, self)
	gridPosition = targetPos
	_state = State.WALKING


func _nextActionAttackingThisPoint(point: Vector2) -> void:
	if _moveTowardsPointIfAway(point):
		return

	var delta = point - gridPosition
	_direction = _deltaToDir(delta)
	_state = State.ATTACKING


# If not adjacent to point, move towards it (either walking or digging). Returns
# A Boolean telling if walked (or dug) or not. IOW, returns false only if
# adjacent.
func _moveTowardsPointIfAway(point: Vector2) -> bool:
	if gridPosition.distance_squared_to(point) <= 1.0:
		return false

	var delta = _deltaTo(point)
	var dir = _deltaToDir(delta)
	var target = gridPosition + _dirToVec(dir)
	var altDir = _deltaToAltDir(delta)
	var altTarget = gridPosition + _dirToVec(altDir)

	# Walk toward the target
	if isWalkableForEnemy(target):
		_startWalking(dir)
		return true
	if isWalkableForEnemy(altTarget):
		_startWalking(altDir)
		return true
	if _level.isDiggable(target):
		_startDigging(dir)
		return true
	if _level.isDiggable(altTarget):
		_startDigging(altDir)
		return true

	# If we can't do anything reasonable, force the AI to start over. Might
	# happen if an enemy is trying to reach a crowded region (like around a
	# surrounded player or Armadillo)
	_decideAgainInSecs = 0.0
	return true


func _startDigging(dir) -> void:
	_direction = dir
	_state = State.DIGGING


func _deltaTo(target: Vector2) -> Vector2:
	return target - gridPosition


func _deltaToDir(delta: Vector2) -> int:
	assert(delta.length_squared() > 0, "Need a nonzero delta vector")
	if abs(delta.x) > abs(delta.y):
		if delta.x > 0:
			return G.Direction.EAST
		else:
			return G.Direction.WEST
	else:
		if delta.y > 0:
			return G.Direction.SOUTH
		else:
			return G.Direction.NORTH


func _deltaToAltDir(delta: Vector2) -> int:
	assert(delta.length_squared() > 0, "Need a nonzero delta vector")
	if abs(delta.x) <= abs(delta.y):
		if delta.x > 0:
			return G.Direction.EAST
		else:
			return G.Direction.WEST
	else:
		if delta.y > 0:
			return G.Direction.SOUTH
		else:
			return G.Direction.NORTH


# Enemies should not walk over the Armadillo. Clumsy code design.
func isWalkableForEnemy(target: Vector2) -> bool:
	return _level.isWalkable(target) && \
		target.distance_squared_to(_level.getArmadilloGridPos()) > 0.1
