extends Node2D


var whyAwayFromArmadillo := """
Time flies when we are having fun, doesn't it?

Armadillo drilled through the next level of the Rabbit Hole, but you were away from it.

Now the Armadillo is there and you are here -- soon to starve to death.
"""

var whyArmadilloDestroyed := """
The cave bugs have bitten the Armadillo until there is no more Armadillo.

Now you are stuck in this level of the Rabbit Hole forever.

Well, not really forever. Those bugs also bite people, and you can hear them approaching...
"""

var whyPlayerKilled := """
Eaten by bugs...

What an inadequate way for an explorer to die.
"""


func onPush(what: int) -> void:
	match what:
		G.LevelResult.AWAY_FROM_ARMADILLO:
			$WhyLabel.text = whyAwayFromArmadillo
		G.LevelResult.ARMADILLO_DESTROYED:
			$WhyLabel.text = whyArmadilloDestroyed
		G.LevelResult.PLAYER_KILLED:
			$WhyLabel.text = whyPlayerKilled
		_:
			assert(false, "Can't happen")


func _onBtnOKPressed() -> void:
	SceneStack.pop()
