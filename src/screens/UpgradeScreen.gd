extends Node2D


func _ready() -> void:
	$Panel/MessageLabel.text = "You survived level %d!" % (G.level - 1)


func _process(delta: float) -> void:
	$Panel/OreLabel.text = "Ore available: %d" % G.ore

	# Enable/disable buttons
	$Panel/BtnFixArmadillo.disabled = G.ore < 1 || G.armadilloHP >= G.maxArmadilloHP
	$Panel/BtnArmadilloUpgradeArmor.disabled = G.ore < 5
	$Panel/BtnArmadilloUpgradeDrill.disabled = G.ore < 3
	$Panel/BtnFixYourself.disabled = G.ore < 1 || G.playerHP >= G.maxPlayerHP
	$Panel/BtnUpgradeYourArmor.disabled = G.ore < 3
	$Panel/BtnUpgradeYourWeapon.disabled = G.ore < 2
	$Panel/BtnUpgradeYourPick.disabled = G.ore < 2

	# Update labels
	if G.armadilloHP < G.maxArmadilloHP:
		var fixed = min(G.armadilloHP + _armadilloFixAmount(), G.maxArmadilloHP)
		$Panel/BtnFixArmadillo/Instructions.text = "Armadillo HP from %d to %d" % [ G.armadilloHP, fixed]
	else:
		$Panel/BtnFixArmadillo/Instructions.text = "Armadillo is fully fixed"

	if G.playerHP < G.maxPlayerHP:
		var fixed = min(G.playerHP + _playerFixAmount(), G.maxPlayerHP)
		$Panel/BtnFixYourself/Instructions.text = "Your HP from %d to %d" % [ G.playerHP, fixed]
	else:
		$Panel/BtnFixYourself/Instructions.text = "Your armor is fully fixed"

	$Panel/BtnArmadilloUpgradeArmor/Instructions.text = "Armadillo Max HP from %d to %d" % [ G.maxArmadilloHP, G.maxArmadilloHP + _armadilloUpgradeArmorAmount()]
	$Panel/BtnUpgradeYourArmor/Instructions.text = "Your Max HP from %d to %d" % [ G.maxPlayerHP, G.maxPlayerHP + _playerUpgradeArmorAmount()]


func _onBtnNextLevelPressed() -> void:
	G.diggingTimeInSecs /= G.drillSpeed
	SceneStack.pop()


func _onBtnFixArmadilloPressed() -> void:
	G.ore -= 1
	G.armadilloHP += _armadilloFixAmount()
	G.armadilloHP = min(G.armadilloHP, G.maxArmadilloHP)


func _onBtnArmadilloUpgradeArmorPressed() -> void:
	G.ore -= 5
	var oldMax := G.maxArmadilloHP
	G.maxArmadilloHP += _armadilloUpgradeArmorAmount()
	G.armadilloHP += G.maxArmadilloHP - oldMax


func _onBtnArmadilloUpgradeDrillPressed() -> void:
	G.ore -= 3
	G.drillSpeed *= 1.15


func _onBtnFixYourselfPressed() -> void:
	G.ore -= 1
	G.playerHP += _playerFixAmount()
	G.playerHP = min(G.playerHP, G.maxPlayerHP)


func _onBtnUpgradeYourArmorPressed() -> void:
	G.ore -= 2
	var oldMax := G.maxPlayerHP
	G.maxPlayerHP += _playerUpgradeArmorAmount()
	G.playerHP += G.maxPlayerHP - oldMax


func _onBtnUpgradeYourWeaponPressed() -> void:
	G.ore -= 2
	G.knifeDamagePerSec *= 1.15


func _onBtnUpgradeYourPickPressed() -> void:
	G.ore -= 2
	G.pickDamagePerSec *= 1.15


func _armadilloFixAmount() -> int:
	return int(G.maxArmadilloHP * 0.2)


func _playerFixAmount() -> int:
	return int(G.maxPlayerHP * 0.4)


func _armadilloUpgradeArmorAmount() -> int:
	return int(G.maxArmadilloHP * 0.3)


func _playerUpgradeArmorAmount() -> int:
	return int(G.maxPlayerHP * 0.3)
