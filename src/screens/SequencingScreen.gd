extends Node2D


func _ready() -> void:
	SceneStack.setInitialScene(self)
	G.resetGlobalStateForNewGame()
	SceneStack.push("res://screens/PlayingScreen.tscn")


func onDigOut(result) -> void:
	match result:
		G.LevelResult.SUCCESS:
			if G.level == G.FINAL_LEVEL:
				_playerWon()
			else:
				_nextLevel()

		G.LevelResult.AWAY_FROM_ARMADILLO, G.LevelResult.PLAYER_KILLED, G.LevelResult.ARMADILLO_DESTROYED:
			_gameOver(result)

		_:
			assert(false, "Can't happen")


func _nextLevel():
	G.level += 1
	G.diggingTimeInSecs = G.drillingTime(G.level) # drill speed taken into account in the upgrade screen
	SceneStack.push("res://screens/PlayingScreen.tscn")
	SceneStack.push("res://screens/UpgradeScreen.tscn")


func _playerWon():
	SceneStack.replaceTop("res://screens/VictoryScreen.tscn")


func _gameOver(reason):
	SceneStack.replaceTop("res://screens/GameOverScreen.tscn", reason)
