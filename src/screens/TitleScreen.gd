extends Node2D


func _ready() -> void:
	SceneStack.setInitialScene(self)


func _on_BtnPlayPressed() -> void:
	SceneStack.push("res://screens/SequencingScreen.tscn")


func _onBtnInstructionsPressed() -> void:
	SceneStack.push("res://screens/InstructionsScreen.tscn")
