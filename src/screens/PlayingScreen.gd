extends Node2D

onready var prevArmadilloHP := G.armadilloHP

func _onDiggingTimerTimeout() -> void:
	G.diggingTimeInSecs -= 1

	if G.diggingTimeInSecs <= 15 && !$AlmostDrilledPlayer.playing:
		$AlmostDrilledPlayer.play()

	if G.diggingTimeInSecs <= 0:
		_digToNextLevel()


func _digToNextLevel() -> void:
	if $Player.gridPosition == Vector2(0, 0):
		SceneStack.pop(G.LevelResult.SUCCESS)
	else:
		SceneStack.pop(G.LevelResult.AWAY_FROM_ARMADILLO)


func _onAlarmTimerTimeout() -> void:
	if G.armadilloHP < prevArmadilloHP:
		$AlarmPlayer.play()
		$HUD.doArmadilloAlarm()
	prevArmadilloHP = G.armadilloHP
