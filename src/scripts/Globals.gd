extends Node

const TILE_SIZE := 80
const FINAL_LEVEL := 7


enum LevelResult {
	SUCCESS,
	AWAY_FROM_ARMADILLO,
	ARMADILLO_DESTROYED,
	PLAYER_KILLED
}

enum Direction {
	NORTH,
	SOUTH,
	EAST,
	WEST,
}


enum DigResult {
	HIT_NOTHING,
	HIT_INDESTRUCTIBLE,
	HIT_DESTRUCTIBLE,
	HIT_AND_DESTROYED,
}


enum AttackResult {
	HIT_NOTHING,
	HIT_ENEMY,
	HIT_WALL,
	HIT_AND_KILLED,
}


enum EnemyAttackResult {
	HIT_NOTHING,
	HIT_PLAYER,
	HIT_ARMADILLO,
	HIT_WALL,
}


# Global state. Initialize to crazy values to make obvious I am doing the proper
# call to initialize everything.
var level: int = -171
var diggingTimeInSecs: int = 999999
var pickDamagePerSec: float = -1.0
var knifeDamagePerSec: float = -1.0
var playerHP: float = -10
var maxPlayerHP: float = -10
var armadilloHP: float = -10
var maxArmadilloHP: float = -10
var ore: int = -171
var oil: float = -1.0
var drillSpeed = -1.0


func resetGlobalStateForNewGame() -> void:
	level = 1
	diggingTimeInSecs = drillingTime(level)
	pickDamagePerSec = 10.0
	knifeDamagePerSec = 10.0
	maxPlayerHP = 12
	maxArmadilloHP = 50
	playerHP = maxPlayerHP
	armadilloHP = maxArmadilloHP
	ore = 0
	oil = 1.5
	drillSpeed = 1.0


# Get the drilling time in seconds for a given level lvl.
func drillingTime(lvl: int) -> int:
	return int(30 * pow(1.3, lvl - 1))
