extends Node2D

export var level: NodePath
onready var _level: BaseLevel = get_node(level)

const SPEED = 200
const OIL_PER_SEC = 0.01

enum State {
	IDLE,
	WALKING,
	ATTACKING,
	DIGGING,
	UNCONTROLLABLE, # e.g. when transitioning between levels
}

var _state = State.IDLE
var _direction = G.Direction.EAST
var gridPosition := Vector2(0, 0) # integer indices into grid


func _process(delta: float) -> void:
	resizeLight(delta)
	rotateTools()

	match _state:
		State.IDLE:
			_processIdle(delta)
		State.WALKING:
			_processWalking(delta)
		State.ATTACKING:
			_processAttacking(delta)
		State.DIGGING:
			_processDigging(delta)
		State.UNCONTROLLABLE:
			pass
		_:
			assert(false, "Can't happen")


func _processIdle(_delta: float) -> void:
	if Input.is_action_pressed("attack"):
		_state = State.ATTACKING
	elif Input.is_action_pressed("dig"):
		_state = State.DIGGING
	elif Input.is_action_pressed("walk_north"):
		_startWalking(G.Direction.NORTH)
	elif Input.is_action_pressed("walk_south"):
		_startWalking(G.Direction.SOUTH)
	elif Input.is_action_pressed("walk_east"):
		_startWalking(G.Direction.EAST)
	elif Input.is_action_pressed("walk_west"):
		_startWalking(G.Direction.WEST)


func _processWalking(delta: float) -> void:
	var target := gridPosition * G.TILE_SIZE
	var move: Vector2 = (target - position).normalized() * SPEED * delta
	var squaredDist = position.distance_squared_to(target)
	if squaredDist < 5.0 || squaredDist < move.length_squared():
		position = target
		_state = State.IDLE
		_level.getResourceMaybe(gridPosition)
	else:
		position += move


func _processDigging(delta: float):
	if !Input.is_action_just_pressed("dig"):
		$AnimationPlayer.play("Dig")
		if !$PickHitPlayer.playing:
			$PickHitPlayer.play()
		$Tools/Pick.visible = true


	if !Input.is_action_pressed("dig"):
		_state = State.IDLE
		$AnimationPlayer.stop(true)
		$Tools/Pick.visible = false
		$PickHitPlayer.stop()
		return

	var target = gridPosition + _dirToVec(_direction)
	_level.playerDigAt(target, delta)

	if Input.is_action_pressed("walk_north"):
		_direction = G.Direction.NORTH
	elif Input.is_action_pressed("walk_south"):
		_direction = G.Direction.SOUTH
	elif Input.is_action_pressed("walk_east"):
		_direction = G.Direction.EAST
	elif Input.is_action_pressed("walk_west"):
		_direction = G.Direction.WEST



func _processAttacking(delta: float):
	if !Input.is_action_just_pressed("attack"):
		$AnimationPlayer.play("Attack")
		if !$AttackPlayer.playing:
			$AttackPlayer.play()

		$Tools/Knife.visible = true

	if !Input.is_action_pressed("attack"):
		_state = State.IDLE
		$AnimationPlayer.stop(true)
		$AttackPlayer.stop()
		$Tools/Knife.visible = false
		return

	var target = gridPosition + _dirToVec(_direction)
	_level.playerAttackAt(target, delta)

	if Input.is_action_pressed("walk_north"):
		_direction = G.Direction.NORTH
	elif Input.is_action_pressed("walk_south"):
		_direction = G.Direction.SOUTH
	elif Input.is_action_pressed("walk_east"):
		_direction = G.Direction.EAST
	elif Input.is_action_pressed("walk_west"):
		_direction = G.Direction.WEST


func _startWalking(dir) -> void:
	_direction = dir
	var dirVec := _dirToVec(dir)
	var targetPos = gridPosition + dirVec
	if !_level.isWalkable(targetPos):
		return

	gridPosition = targetPos
	_level.setPlayerGridPos(gridPosition)
	_state = State.WALKING


func resizeLight(delta: float) -> void:
	G.oil -= OIL_PER_SEC * delta
	if G.oil < 0.0:
		G.oil = 0.0

	var scale := (G.oil+1) / 2.0
	scale = clamp(scale, 0.5, 6.0)
	$Light.texture_scale = scale


func _dirToVec(dir) -> Vector2:
	match dir:
		G.Direction.NORTH:
			return Vector2(0, -1)
		G.Direction.SOUTH:
			return Vector2(0, 1)
		G.Direction.EAST:
			return Vector2(1, 0)
		G.Direction.WEST:
			return Vector2(-1, 0)
		_:
			assert(false, "Can't happen")
			return Vector2(0, 0)


func rotateTools() -> void:
	match _direction:
		G.Direction.NORTH:
			$Tools.rotation_degrees = 0
		G.Direction.SOUTH:
			$Tools.rotation_degrees = 180
		G.Direction.EAST:
			$Tools.rotation_degrees = 90
		G.Direction.WEST:
			$Tools.rotation_degrees = 270
		_:
			assert(false, "Can't happen")
